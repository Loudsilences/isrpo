﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            RoflClass roflclass = new RoflClass();
            Test test = new Test();

            // Умножение проверка
            test.unit(roflclass.quadrat(2, 4), 16);
            test.unit(roflclass.quadrat(1, 4), 12);
            test.unit(roflclass.quadrat(4, 2), 16);

            // Умножение
            test.unit(roflclass.multiply(3, 3), 9);
            test.unit(roflclass.multiply(3, 9), 27);
            test.unit(roflclass.multiply(2, 2), 42);

            // Деление
            test.unit(roflclass.division(10, 5), 2);
            test.unit(roflclass.division(5, 2), 2.25);

            //Сложение
            test.unit(roflclass.summa(2, 2), 4);
            test.unit(roflclass.summa(10, 10), 20);
            test.unit(roflclass.summa(5, 5), 10);

            // Вычитание
            test.unit(roflclass.minus(5, 5), 0);
            test.unit(roflclass.minus(50, 20), 30);
            test.unit(roflclass.minus(20, 40), -20);

            Console.ReadKey();
        }
        public class RoflClass
        {
            public double quadrat(float num, int power)
            {
                return Math.Pow(num, power);
            }

            public double multiply(double x, double y)
            {
                return x * y;
            }

            public double division(double x, double y)
            {
                return x / y;
            }
            public double summa(double x, double y)
            {
                return x + y;
            }
            public double minus(double x, double y)
            {
                return x - y;
            }
        }


        public class Test
        {
            public void unit(double result, double correct)
            {
                if (result == correct) Console.WriteLine("Тест пройден успешно");
                else Console.WriteLine("Тест провален... Ожидалось " + correct + " получено " + result);
            }

            public void unit(int result, int correct)
            {
                if (result == correct) Console.WriteLine("Тест пройден успешно");
                else Console.WriteLine("Тест провален... Ожидалось " + correct + " получено " + result);
            }

            public void unit(string result, string correct)
            {
                if (result == correct) Console.WriteLine("Тест пройден успешно");
                else Console.WriteLine("Тест провален... Ожидалось " + correct + " получено " + result);
            }

        }
    }
}